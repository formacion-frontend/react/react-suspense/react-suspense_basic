import { useEffect, useRef, useState } from "react";

export const useLazyLoadOneComponent = ({ distance = "100" }) => {
  const [isNearScreen, setIsNearScreen] = useState(false);
  const fromRef = useRef();

  useEffect(() => {
    const onChange = (entries) => {
      const elemento1 = entries[0];
      if (elemento1.isIntersecting) {
        setIsNearScreen(true);
        observer.disconnect();
      }
    };
    const observer = new IntersectionObserver(onChange, {
      rootMargin: `${distance}px`,
    });
    observer.observe(fromRef.current);
  });

  return { isNearScreen, fromRef };
};
