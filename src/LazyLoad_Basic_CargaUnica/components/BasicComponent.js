import React, { Suspense } from "react";
import "antd/dist/antd.css";
import { useLazyLoadOneComponent } from "../hooks/useLazyLoadOneComponent";
import { CaraTriste } from "./CaraTriste";
import { Loading } from "./Loading";

const CaraFeliz = React.lazy(() => import("./CaraFeliz")); //REALIZAMOS UN IMPORT DINAMICO , SOLO HARA EL IMPORT DEL COMPONENTE CUANDO ESTE LO REQUIERA NO SE CARGA SIEMPRE AL LEER EL COMPONENTE BASICCOMPONENTS.
//***************                 ES IMPORTANTISIMO QUE EL COMPONENTE QUE SE LE PASA COMO DINAMICO SE DEFINA COMO "export default function nombreComponente (){*contenido componente}"  Y QUE LA CONST SE LLAME IGUAL QUE EL COMPONENTE  ************ */

export const LazyBasicComponent = () => {
  const { isNearScreen, fromRef } = useLazyLoadOneComponent({ distance: -420 });

  return (
    <div ref={fromRef}>
      <Suspense fallback={<Loading />}>
        {" "}
        {/*SUSPENSE ES NECESARIO PARA CARGAS DINAMICAS Y NOS ASEGURA QUE HAYA ALGO EN PANTALLA MIENTRAS SE CARGAN LOS IMPORTS DEL COMPONENTE CARGADO DE FORMA DINAMICA */}
        {isNearScreen ? <CaraFeliz /> : <CaraTriste />}
        {/*EL COMPONENTE CaraFeliz SOLO IMPORTA EL CONTENIDO CUANDO SE VISUALIZA GRACIAS AL SUSPENSE Y REACT.LAZY*/}
      </Suspense>
    </div>
  );
};
