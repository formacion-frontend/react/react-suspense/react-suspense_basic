import React from "react";
import { Result, Spin } from "antd";
import { QuestionOutlined } from "@ant-design/icons";

export const Loading = () => {
  return (
    <>
      <Result
        icon={<QuestionOutlined />}
        title="Se esta cargando el componente dinamico, estará feliz la carita?"
      />
      <Spin size="large" />
    </>
  );
};
