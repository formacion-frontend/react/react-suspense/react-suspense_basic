import React from "react";
import { Result } from "antd";
import { FrownOutlined } from "@ant-design/icons";

export const CaraTriste = () => {
  const texto =
    "*PARA VER EL EFECTO DEL SUSPENSE Y CARGA DE COMPONENTE DINAMICO PONER LA VELOCIDAD DE RED EN CONSOLA A LA MAS LENTA POSIBLE*  El Lazy component sigue sin estar cargado!!";
  return (
    <>
      <Result icon={<FrownOutlined />} title={texto} />
    </>
  );
};
