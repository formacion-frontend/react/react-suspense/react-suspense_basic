import React from "react";
import { Result } from "antd";
import { SmileOutlined } from "@ant-design/icons";

export default function CaraFeliz() {
  //ES NECESARIO DEFINIRLO COMO UNA FUNCION POR QUE SERA CARGADO DE FORMA DINAMICA POR REACT.LAZY
  return (
    <>
      <Result
        icon={<SmileOutlined />}
        title="El componente se ha cargado con exito, el Lazy Load ha funcionado correctamente cargando solo la cara feliz cuando has bajado hasta lo indicado en este caso cuando se muestra el div verde."
      />
    </>
  );
}
