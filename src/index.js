import React from "react";
import ReactDOM from "react-dom";
import "./styles/index.css";
import { LazyLoadCargaUnica } from "./LazyLoad_Basic_CargaUnica/LazyLoadCargaUnica";

ReactDOM.render(
  <React.StrictMode>
    <LazyLoadCargaUnica />
  </React.StrictMode>,
  document.getElementById("root")
);
